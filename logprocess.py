#!/usr/bin/env python3
import os
import sys
import argparse
import traceback
import yaml
import json
yaml.Dumper.ignore_aliases = lambda *args : True

def parse_args(argv):
    self_name = os.path.basename(argv[0])
    epilog = '''\
Supported actions:
  extract-main-fields-plain - extract basic statistics from log file
  average-response-time - extract average response time by path
'''
    parser = argparse.ArgumentParser(prog=self_name,
                                     description='Specifical log processing',
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     epilog=epilog)
    parser.add_argument('-a', '--action', type=str, help='Action')
    parser.add_argument('-i', type=str, help='Input file to process')
    return parser.parse_args(sys.argv[1:])

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def eprint_exception(e, print_traceback=True, need_exit=True):
    eprint(e)
    if print_traceback:
        eprint(traceback.format_exc())
    if need_exit:
        exit(1)

def basic_stats(args, basic_config):
    ret = []
    with open(args.i, 'r') as f:
        for line in f:
            line_obj = json.loads(line)
            request_time_raw = float(line_obj['request_time'])
            request_time_int = int(round(request_time_raw))
            n = 10
            request_time_int_10 = int(n * round(float(request_time_int)/n))
            request_method = line_obj['request_method']
            request_uri = line_obj['request_uri']
            request_uri_noparam = line_obj['request_uri'].split('?')[0]
            print(f'{request_time_int_10}|{request_method}|{request_uri}|{request_uri_noparam}')
    return ret

def average_response_time(args, basic_config):
    ret = {}
    with open(args.i, 'r') as f:
        for line in f:
            line_obj = json.loads(line)
            request_time_raw = float(line_obj['request_time'])
            request_method = line_obj['request_method']
            request_uri_noparam = line_obj['request_uri'].split('?')[0]
            key = f'{request_method} {request_uri_noparam}'
            if key not in ret:
                ret[key] = {'sum_time': request_time_raw, 'count': 1}
            else:
                ret[key]['sum_time'] = ret[key]['sum_time'] + request_time_raw
                ret[key]['count'] = ret[key]['count'] + 1
    for key in ret:
        ret[key]['average_time'] = ret[key]['sum_time'] / ret[key]['count']
    return ret

def main():
    dump_res = True
    args = parse_args(sys.argv)
    if args.i is None:
        print('Input file name is required')
        return 1
    basic_config = {
        'some_key': 1
    }
    res = []
    try:
        if args.action is None or args.action == 'extract-main-fields-plain':
            basic_stats(args, basic_config)
            dump_res = False
        elif args.action == 'average-response-time':
            res = average_response_time(args, basic_config)
            for key in res:
                print('{}|{}'.format(key, res[key]['average_time']))
            dump_res = False
    except OSError as err:
        print("OS error: ", err)
    except ValueError:
        print("Could not process data: ", err)
    except Exception as err:
        print(f"Unexpected {err=}, {type(err)=}")
    else:
        if dump_res:
            yaml.dump(res, sys.stdout, default_flow_style=False, allow_unicode=True)

if __name__ == '__main__':
    main()